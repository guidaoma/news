package com.blog.mapper;


import com.blog.pojo.RawNews;
import com.blog.pojo.SimpleNews;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
public interface RawNewsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RawNews record);

    int insertSelective(RawNews record);

    RawNews selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RawNews record);

    int updateByPrimaryKey(RawNews record);

    List<RawNews> getAllRawNews();

    List<RawNews> queryRawNewsByLimit(@Param("start") int start, @Param("pageSize") int pageSize);

    List<RawNews> queryRawNewsByTitleLimit(@Param("title") String title,@Param("start") int start,@Param("pageSize") int pageSize);

    List<RawNews> getUnusedNews();

    Boolean setRawNewsUsed(@Param("id") int id);

    String getUrlById(@Param("id") int id);
}