package com.blog.mapper;

import com.blog.pojo.chat.ChatUser;
import com.blog.pojo.chat.Staff;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ChatMapper {
    Staff findByUsername(@Param("name") String name);

    Staff findByStaffId(@Param("id") long id);

    int addChatUser(@Param("staff") Staff staff);

    Long findIdByUsername(@Param("name") String name);
}
