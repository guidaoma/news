package com.blog.service;


import com.blog.pojo.Article;
import com.blog.pojo.RawNews;
import com.blog.pojo.SimpleNews;

import java.util.List;


public interface RawNewsService{


    int deleteByPrimaryKey(Integer id);

    int insert(RawNews record);

    int insertSelective(RawNews record);

    RawNews selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(RawNews record);

    int updateByPrimaryKey(RawNews record);

    List<RawNews> getAllRawNews();

    List<RawNews> queryRawNewsByLimit(int start, int pageSize);

    List<RawNews> queryRawNewsByTitleLimit(String title,int start,int pageSize);;

    List<RawNews> getUnusedNews();

    Boolean setRawNewsUsed(int id);


    String getUrlById(int id);
}
