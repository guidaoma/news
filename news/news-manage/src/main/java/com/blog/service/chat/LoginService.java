package com.blog.service.chat;


import com.blog.pojo.chat.ChatUser;
import com.blog.pojo.chat.Staff;

public interface LoginService {


    String getpwdbyname(String name);


    Long getUidbyname(String name);


    String getnamebyid(long id);

    int addChatUser(Staff staff);
}
