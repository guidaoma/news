package com.blog.service.chat.impl;



import com.blog.mapper.ChatMapper;
import com.blog.pojo.chat.ChatUser;
import com.blog.pojo.chat.Staff;
import com.blog.service.chat.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;


@Service("loginservice")
public class LoginServiceImpl implements LoginService {

    @Resource
	ChatMapper chatMapper;

    @Override
	public String getpwdbyname(String name) {
        Staff s = chatMapper.findByUsername(name);
        if (s != null) {
			return s.getPassword();
		} else {
			return null;
		}
    }

    @Override
	public Long getUidbyname(String name) {
		Long id = chatMapper.findIdByUsername(name);
    	return id;
    }

    @Override
	public String getnamebyid(long id) {
        Staff s = chatMapper.findByStaffId(id);
        if (s != null) {
			return s.getUsername();
		} else {
			return null;
		}
    }

	@Override
	public int addChatUser(Staff staff) {
		int result = chatMapper.addChatUser(staff);
		return result;
	}


}
