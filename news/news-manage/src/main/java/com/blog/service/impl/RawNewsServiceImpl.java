package com.blog.service.impl;

import com.blog.pojo.Article;
import com.blog.pojo.RawNews;
import com.blog.pojo.SimpleNews;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.blog.mapper.RawNewsMapper;
import com.blog.service.RawNewsService;

import java.util.List;


@Service
public class RawNewsServiceImpl implements RawNewsService{

    @Resource
    private RawNewsMapper RawNewsMapper;

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return RawNewsMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(RawNews record) {
        return RawNewsMapper.insert(record);
    }

    @Override
    public int insertSelective(RawNews record) {
        return RawNewsMapper.insertSelective(record);
    }

    @Override
    public RawNews selectByPrimaryKey(Integer id) {
        return RawNewsMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKeySelective(RawNews record) {
        return RawNewsMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(RawNews record) {
        return RawNewsMapper.updateByPrimaryKey(record);
    }

    @Override
    public List<RawNews> getAllRawNews() {
        return RawNewsMapper.getAllRawNews();
    }

    @Override
    public List<RawNews> queryRawNewsByLimit(int start, int pageSize) {
        return RawNewsMapper.queryRawNewsByLimit(start,pageSize);
    }

    @Override
    public List<RawNews> queryRawNewsByTitleLimit(String title, int start, int pageSize) {
        return RawNewsMapper.queryRawNewsByTitleLimit(title,start,pageSize);
    }

    @Override
    public List<RawNews> getUnusedNews() {
        return RawNewsMapper.getUnusedNews();
    }

    @Override
    public Boolean setRawNewsUsed(int id) {
        return RawNewsMapper.setRawNewsUsed(id);
    }

    @Override
    public String getUrlById(int id) {
        String s = "http://localhost:8080/";
        String url = RawNewsMapper.getUrlById(id);
        if (url == null){
            return s;
        }else{
            return url;
        }
    }


}
