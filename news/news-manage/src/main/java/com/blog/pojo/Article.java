package com.blog.pojo;

import lombok.Data;
import java.util.Date;

import static com.blog.utills.MyUtils.formatTime;


@Data
public class Article {

    //文章id  主键
    private int id;

    //文章作者
    private String author;

    //文章标题
    private  String articleTitle;

    //文章内容
    private String articleContent;

    //文章类型
    private String articleType;

    //文章标签
    private String articleTags;

    //创建时间
    private String publishDate;

    //最后一次更改时间
    private String updateDate;

    //文章喜欢数
    private int likes;

    //外键，用于查询url
    private int rawnewsid;


    public Article(String author, String articleTitle, String articleContent, String articleType, String articleTags,int rawnewsid) {
        this.author = author;
        this.articleTitle = articleTitle;
        this.articleContent = articleContent;
        this.articleType = articleType;
        this.articleTags = articleTags;
        //自己传当前的时间
        this.updateDate = formatTime(new Date());
        this.rawnewsid = rawnewsid;
    }

    public Article(String author, String articleTitle, String articleContent, String articleType, String articleTags) {
        this.author = author;
        this.articleTitle = articleTitle;
        this.articleContent = articleContent;
        this.articleType = articleType;
        this.articleTags = articleTags;
        //自己传当前的时间
        this.updateDate = formatTime(new Date());
        this.rawnewsid = 0;
    }

    public Article() {

    }
}
