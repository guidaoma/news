package com.blog.pojo.chat;

/**
 * 存储私聊用户，和User表一样。
 * 但是只有id,name,pwd
 */
public class Staff {

	private Integer staff_id;
	private String username;
	private String password;

	public Integer getStaff_Id() {
		return staff_id;
	}

	public void setStaff_Id(Integer staff_Id) {
		this.staff_id = staff_Id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}