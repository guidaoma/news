package com.blog.pojo;

import lombok.Data;

@Data
public class LeaveMessage {

    //投稿id
    private int id;

    //投稿标题
    private String title;

    //投稿内容
    private String content;

    //投稿人姓名
    private String name;

    //投稿人邮箱
    private String email;

    //投稿时间
    private String time;

}
