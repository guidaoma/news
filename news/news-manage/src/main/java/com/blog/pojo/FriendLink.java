package com.blog.pojo;

import lombok.Data;

@Data
public class FriendLink {

    //友联id
    private int id;

    //博客名字
    private String  blogName;

    //博客链接
    private String url;

}
