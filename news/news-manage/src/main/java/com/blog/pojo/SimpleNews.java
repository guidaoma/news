package com.blog.pojo;

import lombok.Data;

import java.io.Serializable;

/**
 */
@Data
public class SimpleNews implements Serializable {
    private String id;
    private String content_html;
    private String url; //新闻原链接
    private String title; //新闻标题，可以当成新闻主体
    private String date_modified; //时间，格式为2020-12-19T04:05:43.000Z
}

/**
 * {
 *       "id": 1936318,
 *       "content_html": "",
 *       "url": "https://finance.sina.cn/7x24/2020-12-19/detail-iiznezxs7755055.d.html",
 *       "title": "【国家能源集团：7项措施全力保障供煤供电供暖】针对近日部分地区煤炭、电力供应偏紧，国家能源集团19日称，切实发挥能源央企稳定器和压舱石作用，采取7项措施全力保障供煤供电供暖。包括加大自产煤生产，切实保障煤炭供应；确保所有供热机组“零临修”，自有供热管网“零故障”，切实保障供电供热；结合用户实际适时调整保供方案，确保居民温暖过冬；尽快开展和完成2021年度煤炭中长期合同签订和履约，稳定市场预期和煤炭价格等。",
 *       "date_modified": "2020-12-19T04:05:43.000Z"
 *     },
 */
