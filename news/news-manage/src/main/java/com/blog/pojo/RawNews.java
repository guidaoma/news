package com.blog.pojo;

import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 存入数据库的pojo
 */
@Data
public class RawNews implements Serializable {
    /**
    * id
    */
    private Integer id;

    /**
    * 标题，可以当成全文
    */
    private String title;

    /**
    * 原文链接
    */
    private String url;

    /**
    * 日期时间
    */
    private Date date;

    /**
    * 使用发表过，0否1是，默认0
    */
    private Integer isused;
}