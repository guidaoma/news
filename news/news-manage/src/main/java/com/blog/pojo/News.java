package com.blog.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 爬取到的原始数据
 * https://ruanyf.github.io/sina-news/rss.json
 * Serializable实现序列化，才能json转为bean
 */
@Data
public class News implements Serializable {
    private String version;
    private String title;
    private String home_page_url;
    private String feed_url;
    private String description;
    private List<SimpleNews> items; //新闻主体，一个数组
}


