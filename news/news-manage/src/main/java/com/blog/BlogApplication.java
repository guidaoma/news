package com.blog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogApplication {
    /**
     * bug1：以注册但是新用户无法登陆
     * 用户管理无法进入 数据库读取上出错    SQLException: Invalid value for getInt()
     * 解决办法：去掉user的lombok
     *
     */
    //TODO 加入swagger生成接口文档  注解实现
    //TODO api请求随机生成头像 https://avatar.prodless.com/ 返回base64
    //TODO 首页没有下一页应该把“下一页”变灰
    //TODO (建议丢弃该功能)文章标签应该是选择不是自己填写(涉及到数据库，需要新添加一张表来存储标签)，同时前端也要修改代码
    //TODO 用户管理处，调整性别错误（比如男就有两个男，应该是1/0性别选择错误-->尝试男0女1）
    //TODO 登陆后，右上无法对齐（可能是前端框架问题，不用管）
    //TODO 后端数据校验 JSR303注解
    //完善md，加入截图
    //上传sql文件，做表分析
    //私聊
    //网络爬取新闻（正文添加上原文链接）
    public static void main(String[] args) {
        SpringApplication.run(BlogApplication.class, args);
    }

}
