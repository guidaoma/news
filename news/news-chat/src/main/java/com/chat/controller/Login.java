package com.chat.controller;


import com.chat.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


@Controller
public class Login {
    @Autowired
    LoginService loginservice;

    @RequestMapping("/loginvalidate")
    public String loginvalidate(@RequestParam("username") String username, @RequestParam("pic") String pic, @RequestParam("password") String pwd, HttpSession httpSession) {
        String picode = (String) httpSession.getAttribute("rand");
        if (null == picode) {
            return "/login";
        }
        if (!picode.equalsIgnoreCase(pic)) {
            return "/failcode";
        }
        if (username == null) {
            return "/login";
        }
        String realpwd = loginservice.getpwdbyname(username);
        if (realpwd.equals(pwd)) {
            long uid = loginservice.getUidbyname(username);
            httpSession.setAttribute("username", username);
            httpSession.setAttribute("uid", uid);
            return "/chatroom";
        } else {
            return "/fail";
        }
    }

    @RequestMapping("login")
    public String login() {
        return "/login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute("username");
        httpSession.removeAttribute("uid");
        return "/login";
    }
}
