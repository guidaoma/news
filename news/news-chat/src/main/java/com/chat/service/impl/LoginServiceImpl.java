package com.chat.service.impl;


import com.chat.mapper.ChatMapper;
import com.chat.po.Staff;
import com.chat.service.LoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("loginservice")
public class LoginServiceImpl implements LoginService {

    @Resource
	ChatMapper chatMapper;

    @Override
	public String getpwdbyname(String name) {
        //Staff s = chatMapper.findByUsername(name);
        String s = chatMapper.findPwdByName(name);
        if (s != null) {
			return s;
		} else {
			return null;
		}
    }

    @Override
	public Long getUidbyname(String name) {
		Long id = chatMapper.findIdByUsername(name);
    	return id;
    }

    @Override
	public String getnamebyid(long id) {
        Staff s = chatMapper.findByStaffId(id);
        if (s != null) {
			return s.getUsername();
		} else {
			return null;
		}
    }

	@Override
	public int addChatUser(Staff staff) {
		int result = chatMapper.addChatUser(staff);
		return result;
	}


}
