package com.chat.service;


import com.chat.po.Staff;

public interface LoginService {


    String getpwdbyname(String name);


    Long getUidbyname(String name);


    String getnamebyid(long id);

    int addChatUser(Staff staff);
}
