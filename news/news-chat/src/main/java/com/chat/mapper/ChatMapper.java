package com.chat.mapper;

import com.chat.po.Staff;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface ChatMapper {
    Staff findByUsername(@Param("name") String name);

    String findPwdByName(@Param("name") String name);

    Staff findByStaffId(@Param("id") long id);

    int addChatUser(@Param("staff") Staff staff);

    Long findIdByUsername(@Param("name") String name);
}
